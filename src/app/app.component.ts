import { Component, HostListener } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService, UserService } from "./services";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent {
  constructor(private userService: UserService, private router: Router, private auth: AuthService) {
    this.onInit();
  }

  @HostListener("window:onUserRefresh", ["$event"])
  onInit(): void {
    if (this.auth.isLogin()) {
      this.userService.fetchAuthUser(true).subscribe(res => {
        if (this.router.url.indexOf("verify-email") >= 0) {
          return;
        }
        if (res.isPaymentAdded && this.router.url === "/payment") {
          this.router.navigate(["/"]);
        } else if (!res.isPaymentAdded && this.router.url !== "/payment") { 
          this.router.navigate(["payment"]);
        }
      });
    }
  }
}
