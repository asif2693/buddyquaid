import { Route } from "@angular/router";
import { AuthGuard } from "./services";
import { LoginComponent, SignupComponent, ForgotPasswordComponent, ResetPasswordComponent, VerifyEmailComponent } from "./auth";
import { PaymentComponent, ListingComponent, TermsComponent } from "./pages";
import { FaqsComponent, ContactUsComponent, AgentsComponent } from "./pages";
import { AgentDetailComponent, LeaseComponent, MessageBoardComponent } from "./pages";
import { HistoryComponent, TourComponent, TourDetailComponent } from "./pages";
import { ProfileComponent, SubmitPropertyComponent, BuyersRequestsComponent } from "./pages";
import { PropertyDetailComponent, MyPropertiesComponent, MyMessagesComponent } from "./pages";
import { PrivacyComponent } from "./pages";

export const routes: Route[] = [
  { path: "", canActivate: [AuthGuard], children: [
    { path: "", component: ListingComponent },
    { path: "lease", component: LeaseComponent },
    { path: "history", component: HistoryComponent },
    { path: "tour", component: TourComponent },
    { path: "profile", component: ProfileComponent },
    { path: "add-property", component: SubmitPropertyComponent },
    { path: "my-messages", component: MyMessagesComponent },
    { path: "my-properties", component: MyPropertiesComponent },
    { path: "edit-property/:id", component: SubmitPropertyComponent },
    { path: "property-detail/:id", component: PropertyDetailComponent },
    { path: "buyers-requests", component: BuyersRequestsComponent },
    { path: "tour-detail/:id", component: TourDetailComponent },
    { path: "message-board", component: MessageBoardComponent },
    { path: "agents", component: AgentsComponent },
    { path: "agent-detail/:id", component: AgentDetailComponent },
    { path: "*", redirectTo: "", pathMatch: "full" }
  ]},
  { path: "login", canActivate: [AuthGuard], component: LoginComponent },
  { path: "signup", canActivate: [AuthGuard], component: SignupComponent },
  { path: "faqs", component: FaqsComponent },
  { path: "terms", component: TermsComponent },
  { path: "privacy", component: PrivacyComponent },
  { path: "verify-email/:id", component: VerifyEmailComponent },
  { path: "contact-us", component: ContactUsComponent },
  { path: "forgot-password", component: ForgotPasswordComponent },
  { path: "reset-password/:id", component: ResetPasswordComponent },
  { path: "payment/:id", component: PaymentComponent },
  { path: "*", redirectTo: "", pathMatch: "full" }
];
