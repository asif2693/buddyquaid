import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Injector } from "@angular/core";
import { HttpModule, Http, RequestOptions, XHRBackend } from "@angular/http";
import { RouterModule } from "@angular/router";
import { FormsModule} from "@angular/forms";

import { AppComponent } from "./app.component";
import { AuthModule } from "./auth";
import { PagesModule } from "./pages";
import { routes } from "./app.routes";

import { AuthGuard, AuthService, httpFactory, UserService, UploadService, SettingService, MiscService } from "./services";

import { PagesServices } from "./pages";
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
@NgModule({
  imports: [
    BrowserModule,
    PagesModule,
    AuthModule,
    RouterModule.forRoot(routes, { useHash: false }),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    GooglePlaceModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AuthGuard,
    AuthService,
    UserService,
    PagesServices,
    UploadService,
    SettingService,
    MiscService,
    { provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, Injector] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
