import { Component } from "@angular/core";
import { AuthService, MiscService } from "../../services";

@Component({
  selector: "app-faqs",
  templateUrl: "faqs.html"
})
export class FaqsComponent {
  isLogin: boolean;
  faqs: any[] = [];
  constructor(private auth: AuthService, private misc: MiscService) {
    this.isLogin = this.auth.isLogin();
    this.getFaqs();
  }

  getFaqs(): void {
    this.misc.getFaqs().subscribe(res => {
      this.faqs = res;
    });
  }
}
