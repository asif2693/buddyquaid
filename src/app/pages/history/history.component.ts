import { Component } from "@angular/core";
import { ListingService } from "../listing";
import { StatusVals } from "../profile/submit-property";
import { PropertyModel } from "../../models";

@Component({
  selector: "app-history",
  templateUrl: "history.html"
})
export class HistoryComponent {
  properties: PropertyModel[];
  allProperties: PropertyModel[];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  tempProperties: PropertyModel[] = [];
  search = "date";
  constructor(
    private service: ListingService
  ) {
    this.initProperties();
  }

  initProperties(): void {
    this.service.getProperties({
      status: 1,
      statusValue: StatusVals.join(",")
    }).subscribe(props => {
      this.properties = props;
      this.allProperties = props.slice();
      this.resetPages();
      this.changePage(1);
    });
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.properties.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  onSearchApplied(): void {
    setTimeout(() => {
      this.tempProperties.sort((a: any, b: any) => {
        if (this.search === "date") return b.date - a.date;
        if (this.search === "price") return b.price - a.price;
        if (this.search === "status") {
          if (a.status < b.status) return -1;
          if (a.status > b.status) return 1;
          return 0;
        }
        if (this.search === "agent") {
          if (a.firstname < b.firstname) return -1;
          if (a.firstname > b.firstname) return 1;
          return 0;
        }
      });
    });
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.tempProperties = this.properties.slice().splice(this.pageStart, this.pageLimit);
      this.onSearchApplied();
    });
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.properties.length / this.pageLimit)) {
      return `${this.properties.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }
}
