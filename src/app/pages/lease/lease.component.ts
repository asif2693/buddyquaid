import { Component } from "@angular/core";
import { ListingService } from "../listing/listing.service";
import { PropertyModel } from "../../models";
import { StatusVals, StatusValsActive } from "../profile/submit-property";

@Component({
  selector: "app-lease",
  templateUrl: "lease.html"
})
export class LeaseComponent {
  properties: PropertyModel[] = [];
  allProperties: PropertyModel[] = [];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  tempProperties: PropertyModel[] = [];
  isSearchApplied: boolean = false;
  constructor(
    private service: ListingService
  ) {
    this.initProperties();
  }

  initProperties(): void {
    this.service.getProperties({
      lease: true,
      status: 1,
      statusValue: StatusVals.join(",")
    }).subscribe(props => {
      this.properties = props.filter(prp => prp.prop_status !== "Sale");
      this.allProperties = props.slice();
      this.resetPages();
      this.changePage(1);
    });
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.properties.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  onSearchApplied(data): void {
    let properties = this.allProperties.slice();
    if (data.sort_by) {
      properties.sort((a: any, b: any) => {
        if (data.sort_by === "asc") {
          if (a.price_upon_request || b.price_upon_request) {
            return a.price_upon_request ? 0 : 1;
          }
          return b.price - a.price;
        } else {
          if (a.price_upon_request || b.price_upon_request) {
            return a.price_upon_request ? 1 : 0;
          }
          return a.price - b.price;
        }
      });
    }
    this.properties = properties.filter(prop => {
      let FlagList = [];
      if (!prop.price_upon_request) {
        FlagList.push(prop.price >= data.price_min && prop.price <= data.price_max);
      }
      FlagList.push(prop.size_prefix >= data.size_prefix_min && prop.size_prefix <= data.size_prefix_max);
      if (data.city) FlagList.push(data.city == prop.city);
      if (data.bedrooms) FlagList.push(prop.bedrooms >= data.bedrooms);
      if (data.bathrooms) FlagList.push(prop.bathrooms >= data.bathrooms);
      if (data.mls) FlagList.push(prop.mls >= data.mls);
      if (data.prop_type) FlagList.push(prop.prop_type == data.prop_type);
      if (data.features) {
        let temp = [];
        data.features.forEach(feat => {
          temp.push(prop.features.indexOf(feat) >= 0);
        });
        FlagList.push(temp.filter(t => !t).length === 0);
      }
      return FlagList.filter(f => !f).length === 0;
    });
    this.isSearchApplied = true;
    this.resetPages();
    this.changePage(1);
  }

  onClearSearch($event): void {
    this.isSearchApplied = false;
    this.properties = this.allProperties.slice();
    this.resetPages();
    this.changePage(1);
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.tempProperties = this.properties.slice().splice(this.pageStart, this.pageLimit);
    });
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.properties.length / this.pageLimit)) {
      return `${this.properties.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }

  convertToCommas(value) {
    if (!value) return "";
    return `$${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  }
}
