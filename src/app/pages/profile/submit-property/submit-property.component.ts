import { Component, ViewChild, Renderer, ElementRef, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators, ValidationErrors } from "@angular/forms";
import { UserService, AuthService, UploadService, SettingService } from "../../../services";
import { PropertyModel, UserModel } from "../../../models";
import { ProfileService } from "../profile.service";
import { StatusVals, StatusValsActive } from "./data";
import Swal from "sweetalert2";

@Component({
  selector: "app-submit-property",
  templateUrl: "submit-property.html"
})
export class SubmitPropertyComponent implements OnDestroy {
  form: FormGroup;
  user: UserModel;
  @ViewChild("uploadBtn") uploadBtn: ElementRef;
  features: string[] = [];
  files: any[] = [];
  uploadedPhotos: string[] = [];
  subs: any = {};
  isEdit: boolean = false;
  propertyId: string = "";
  settings: any = {
    cities: [],
    features: [],
    mls: [],
    properties: []
  };
  statusVals = StatusVals;
  statusValsActive = StatusValsActive;
  property: any = null;
  constructor(
    private auth: AuthService,
    private service: ProfileService,
    private settingService: SettingService,
    private router: Router,
    private renderer: Renderer,
    private uploadService: UploadService,
    private userService: UserService,
    private activated: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.userService.fetchAuthUser()
    .subscribe(user => {
      this.user = user;
      this.onParams();
    });
  }

  getSettings(prop?: any): void {
    this.settingService.getSettings().subscribe(res => {
      this.settings.cities = res.filter(s => s.type === "city");
      this.settings.features = res.filter(s => s.type === "feature");
      this.settings.mls = res.filter(s => s.type === "mls");
      this.settings.properties = res.filter(s => s.type === "property");
      this.initForm(prop);
    });
  }

  onParams(): void {
    this.subs["params"] = this.activated.params.subscribe(params => {
      if (params.id) {
        this.isEdit = true;
        this.propertyId = params.id;
        this.service.getProperty(this.propertyId).subscribe(prop => {
          this.property = prop;
          this.getSettings(prop);
        });
      } else {
        this.getSettings();
      }
    });
  }

  initForm(prop?: PropertyModel): void {
    if (prop) {
      this.features = prop.features;
      this.form = this.fb.group({
        firstname: [prop.firstname, Validators.required],
        lastname: [prop.lastname, Validators.required],
        profile_image: [prop.profile_image, Validators.required],
        email: [prop.email, Validators.required],
        phone: [prop.phone, Validators.required],
        acres: [prop.acres, Validators.required],
        title : [prop.title, Validators.required],
        location : [prop.location, Validators.required],
        status : [prop.status, Validators.required],
        prop_status : [prop.prop_status, Validators.required],
        price_upon_request: [prop.price_upon_request],
        lease : [prop.lease],
        year_built: [prop.year_built, Validators.required],
        price : [prop.price, [Validators.pattern('[0-9]+')]],
        city : [prop.city, Validators.required],
        prop_type : [prop.prop_type, Validators.required],
        mls : [prop.mls, Validators.required],
        size_prefix : [prop.size_prefix, [Validators.required, Validators.pattern('[0-9]+')]],
        bedrooms : [prop.bedrooms, Validators.required],
        bathrooms : [prop.bathrooms, Validators.required],
        tv_launge : [prop.tv_launge, Validators.required],
        garages : [prop.garages, Validators.required],
        no_swiming_pool : [prop.no_swiming_pool, Validators.required],
        description : [prop.description, Validators.required],
        video_url : [prop.video_url],
        place_on_map : [prop.place_on_map],
        date: [new Date().getTime(), Validators.required]
      });
    } else {
      this.form = this.fb.group({
        firstname: [this.user.firstname, Validators.required],
        lastname: [this.user.lastname, Validators.required],
        profile_image: [this.user.profile_image, Validators.required],
        email: [this.user.email, Validators.required],
        phone: [this.user.phone, Validators.required],
        title : ["", Validators.required],
        location : ["", Validators.required],
        price_upon_request: [false],
        year_built: ["", Validators.required],
        status : ["Active", Validators.required],
        prop_status : ["Sale", Validators.required],
        acres: ["1", Validators.required],
        price : ["", [Validators.pattern('[0-9]+')]],
        size_prefix : ["", [Validators.required, Validators.pattern('[0-9]+')]],
        lease : [false],
        bedrooms : ["1", Validators.required],
        bathrooms : ["1", Validators.required],
        tv_launge : ["1", Validators.required],
        garages : ["1", Validators.required],
        no_swiming_pool : ["No", Validators.required],
        description : ["", Validators.required],
        city : [this.settings.cities[0].title, Validators.required],
        prop_type : [this.settings.properties[0].title, Validators.required],
        mls : [this.settings.mls[0].title, Validators.required],
        video_url : [""],
        place_on_map : [""],
        date: [new Date().getTime(), Validators.required]
      });
      this.uploadedPhotos = [];
      this.files = [];
      this.features = [];
    }
  }

  onFileSelect(dropFiles?: any[]): void {
    let files = dropFiles ? dropFiles : this.uploadBtn.nativeElement.files;
    let reader: any = new FileReader();
    if (files.length > 0) {
      let count = 0;
      for (var i = 0; i < files.length; i++) {
        reader.readAsDataURL(files[i]);
        reader.onload = (e) => {
          this.files.push({
            preview: e.target.result,
            file: files[count++]
          });
          console.log(this.files)
          if (count === files.length) {
            this.uploadBtn.nativeElement.value = "";
          }
        };
      }
    }
  }


  openFileSelect(): void {
    this.renderer.invokeElementMethod(this.uploadBtn.nativeElement, "click");
  }


  toggleFeature(name): void {
    let feat = this.features.filter(f => f === name);
    if (feat.length > 0) {
      this.features.splice(this.features.indexOf(name), 1);
    } else {
      this.features.push(name);
    }
  }

  isFeatureAvailable(name): boolean {
    return this.features.indexOf(name) >= 0;
  }

  saveProperty(): void {
    if (!this.form.valid) {
      this.getFormValidationErrors();
      return;
    }
    if (!this.form.controls["price"].value) {
      this.form.controls["price_upon_request"].setValue(true);
    } else if (this.form.controls["price"].value >= Number.MAX_SAFE_INTEGER) {
      Swal("Error", "Please enter a valid property price", "error");
      return;
    }
    if (this.form.controls["size_prefix"].value >= Number.MAX_SAFE_INTEGER) {
      Swal("Error", "Please enter a valid property size", "error");
      return;
    }
    if (this.files.length === 0 && !this.isEdit) {
      Swal("Error", "Please select minimum 1 image of property", "error");
      return;
    }
    if (this.isEdit) {
      if (this.property.photos.length === 0 && this.files.length === 0) {
        Swal("Error", "Please select minimum 1 image of property", "error");
        return;
      }
    }
    this.uploadPhotosRecursively();
  }

  createProperty(): void {
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    let photos = this.property ? this.property.photos : [];
    req.features = this.features;
    req.photos = this.uploadedPhotos;
    req.photos = req.photos.concat(photos);
    req = this.makeVideoUrl(req);
    if (this.isEdit) {
      req._id = this.propertyId;
      if (StatusVals.indexOf(req.status) >= 0) {
        req.removed_date = new Date().getTime();
      }
      this.service.updateProperty(req).subscribe(res => {
        this.router.navigate(["my-properties"]);
      });
    } else {
      this.service.createProperty(req).subscribe(res => {
        Swal("Success", "Property submitted successfully!", "success");
        this.initForm();
      });
    }
  }

  makeVideoUrl(req): any {
    if (req.video_url) {
      if (req.video_url.indexOf("youtu") >= 0) {
        req.video_type = "youtube";
        if (req.video_url.indexOf("youtu.be") >= 0) {
          req.video_embed_url = req.video_url.slice(req.video_url.lastIndexOf("/") + 1);
        } else {
          req.video_embed_url = req.video_url.slice(req.video_url.lastIndexOf("v=") + 2);
          if (req.video_embed_url.indexOf("&") >= 0) {
            req.video_embed_url = req.video_embed_url.slice(0, req.video_embed_url.indexOf("&"));
          }
        }
      } else if (req.video_url.indexOf("vimeo") >= 0) {
        req.video_type = "vimeo";
        req.video_embed_url = req.video_url.slice(req.video_url.lastIndexOf("/") + 1);
      }
    }
    return req;
  }

  rmvFile(i): void {
    this.files.splice(i, 1);
  }

  rmvUploadedFile(i): void {
    this.property.photos.splice(i, 1);
  }

  uploadPhotosRecursively(idx?: number): void {
    idx = idx ? idx : 0;
    if (this.files.length > 0) {
      this.uploadService.upload(this.files[idx].file).subscribe(res => {
        this.uploadedPhotos.push(`${this.auth.getAPI()}${res.path}`);
        idx++;
        if (idx < this.files.length) {
          this.uploadPhotosRecursively(idx);
        } else {
          this.createProperty();
        }
      });
    } else {
      this.createProperty();
    }
  }

  onDrop($event): void {
    this.onFileSelect($event);
  }

  logout(): boolean {
    this.auth.logout();
    this.router.navigate(["login"]);
    return false;
  }

  getFormValidationErrors() {
    let isError = false;
    Object.keys(this.form.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.form.get(key).errors;
      if (controlErrors != null && !isError) {
        // Object.keys(controlErrors).forEach(keyError => {
        //   console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        // });
        isError = true;
        Swal("Error", this.getErrorMsg(key), "error");
      }
    });
    return isError;
  }

  getErrorMsg(key): string {
    let prefix = "Please enter a valid";
    let errors: any = {
      title: `${prefix} title`,
      location: `${prefix} location`,
      price: `${prefix} price`,
      size_prefix: `${prefix} property size`,
      year_built: `${prefix} year built`,
      description: `Please enter some description`,
      acres: `Please enter acres`
    };
    return errors[key];
  }

  ngOnDestroy(): void {
    for (let key in this.subs) {
      this.subs[key].unsubscribe();
    }
  }
}
