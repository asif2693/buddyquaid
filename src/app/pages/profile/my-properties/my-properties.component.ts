import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { UserService, AuthService } from "../../../services";
import { PropertyModel, UserModel } from "../../../models";
import { ProfileService } from "../profile.service";

@Component({
  selector: "app-my-properties",
  templateUrl: "my-properties.html"
})
export class MyPropertiesComponent {
  properties: PropertyModel[];
  allProperties: PropertyModel[];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  tempProperties: PropertyModel[] = [];
  constructor(
    private auth: AuthService,
    private router: Router,
    private userService: UserService,
    private service: ProfileService
  ) {
    this.initProperties();
  }

  initProperties(): void {
    this.userService.fetchAuthUser().subscribe(user => {
      this.service.getProperties({
        user_id: user._id
      }).subscribe(props => {
      this.properties = props;
      this.allProperties = props.slice();
      this.resetPages();
      this.changePage(1);
      });
    });
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.properties.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.tempProperties = this.properties.slice().splice(this.pageStart, this.pageLimit);
    });
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.properties.length / this.pageLimit)) {
      return `${this.properties.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }

  convertToCommas(value) {
    if (!value) return "";
    return `$${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  }

  logout(): boolean {
    this.auth.logout();
    this.router.navigate(["login"]);
    return false;
  }
}
