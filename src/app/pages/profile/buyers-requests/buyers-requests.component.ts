import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService, AuthService, SettingService, Setting } from "../../../services";
import { PropertyModel } from "../../../models";
import { ListingService } from "../../listing";
import { ProfileService } from "../profile.service";
import { StatusValsActive } from "../submit-property";
import Swal from "sweetalert2";

@Component({
  selector: "app-buyers-requests",
  templateUrl: "buyers-requests.html"
})
export class BuyersRequestsComponent implements AfterViewInit {
  @ViewChild("priceMin") priceMin: ElementRef;
  @ViewChild("priceMax") priceMax: ElementRef;
  @ViewChild("sizeMin") sizeMin: ElementRef;
  @ViewChild("sizeMax") sizeMax: ElementRef;
  @ViewChild("reqResult") reqResult: ElementRef;
  searchedProperties: any[] = [];
  allProperties: PropertyModel[] = [];
  isSearchApplied: boolean = false;
  form: FormGroup;
  settings: any = {
    cities: [],
    features: [],
    mls: [],
    properties: []
  };
  features: string[] = [];
  user: any = {};
  requests: any[] = [];
  constructor(
    private auth: AuthService,
    private router: Router,
    private listingService: ListingService,
    private settingService: SettingService,
    private service: ProfileService,
    private userService: UserService,
    private fb: FormBuilder
  ) {
    this.initData();
    this.getSettings();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      title: [""],
      city: [""],
      mls: [""],
      prop_type: [""],
      bedrooms: [""],
      bathrooms: [""],
      price_min: [0],
      price_max: [1000],
      size_min: [0],
      size_max: [1000],
      description: ["", Validators.required]
    });
  }

  getSettings(): void {
    this.settingService.getSettings().subscribe(res => {
      this.settings.cities = res.filter(s => s.type === "city");
      this.settings.features = res.filter(s => s.type === "feature");
      this.settings.mls = res.filter(s => s.type === "mls");
      this.settings.properties = res.filter(s => s.type === "property");
    });
  }

  initData(): void {
    this.listingService.getProperties({
      status: 1,
      statusValue: StatusValsActive.join(",")
    }).subscribe(props => {
      this.searchedProperties = props.slice();
      this.allProperties = props.slice();
    });
    this.userService.fetchAuthUser().subscribe(user => {
      this.user = user;
      this.service.getBuyerRequests(this.user._id).subscribe(reqs => {
        this.requests = reqs;
      });
    });
  }

  ngAfterViewInit(): void {
    initNSTSlider();
  }

  onSearchApplied(): void {
    let data: any = this.getRequestData();
    this.isSearchApplied = true;
    let properties = this.allProperties.slice();
    this.searchedProperties = properties.filter(prop => {
      let FlagList = [];
      FlagList.push(prop.price >= data.price_min && prop.price <= data.price_max);
      FlagList.push(prop.size_prefix >= data.size_prefix_min && prop.size_prefix <= data.size_prefix_max);
      if (data.city) FlagList.push(data.city == prop.city);
      if (data.bedrooms) FlagList.push(prop.bedrooms >= data.bedrooms);
      if (data.bathrooms) FlagList.push(prop.bathrooms >= data.bathrooms);
      if (data.mls) FlagList.push(prop.mls >= data.mls);
      if (data.prop_type) FlagList.push(prop.prop_type == data.prop_type);
      if (data.features) {
        let temp = [];
        data.features.forEach(feat => {
          temp.push(prop.features.indexOf(feat) >= 0);
        });
        FlagList.push(temp.filter(t => !t).length == 0);
      }
      let matched = FlagList.filter(f => f).length;
      (<any>prop).percent = Math.round((100 / Object.keys(data).length) * matched);
      return matched > 0;
    });
    if (this.searchedProperties.length) {
      setTimeout(() => {
        this.reqResult.nativeElement.scrollIntoView({
          behavior: "smooth"
        });
      }, 100);
    }
  }

  private getRequestData(is_user?: boolean): any {
    let data: any = {};
    if (this.features.length > 0) {
      data.features = this.features;
    }
    for (let key in this.form.controls) {
      if (this.form.controls[key].value) {
        data[key] = this.form.controls[key].value;
      }
    }
    if (is_user) {
      data.user_id = this.user._id;
      data.profile_image = this.user.profile_image;
      data.fullname = `${this.user.firstname} ${this.user.lastname}`;
      data.email = this.user.email;
      data.phone = this.user.phone;
      data.date = new Date().getTime();
    }
    return data;
  }

  toggleFeature(name): void {
    let feat = this.features.filter(f => f === name);
    if (feat.length > 0) {
      this.features.splice(this.features.indexOf(name), 1);
    } else {
      this.features.push(name);
    }
  }

  isFeatureAvailable(name): boolean {
    return this.features.indexOf(name) >= 0;
  }

  submitRequest(): void {
    if (!this.form.valid) {
      Swal("Error", "Please add some description!", "error");
      return;
    }
    let data = this.getRequestData(true);
    if (Object.keys(data).length > 0) {
      this.service.createBuyerRequest(data).subscribe(res => {
        this.initForm();
        this.initData();
        Swal("Success", "Request submitted successfully!", "success");
      });
    }
  }

  convertToCommas(value) {
    if (!value) return "";
    return `$${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  }

  logout(): boolean {
    this.auth.logout();
    this.router.navigate(["login"]);
    return false;
  }
}
