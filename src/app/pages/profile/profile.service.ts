import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs";
import { UserModel, PropertyModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class ProfileService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = this.auth.getAPI();
  }

  getProfile(body: any): Observable<UserModel> {
    return this.http.get(`${this.API}/users/me`, body)
    .map(res => res.json());
  }

  getProperties(params?: any): Observable<PropertyModel[]> {
    params = params ? params : {};
    return this.http.get(`${this.API}/properties`, {
      params: params
    })
    .map(res => res.json())
    .map(res => {
      res.forEach(p => {
        p.date = new Date(parseInt(p.date));
      });
      res.sort((a, b) => b.date - a.date);
      return res;
    });
  }

  getProperty(id: string): Observable<PropertyModel> {
    return this.http.get(`${this.API}/properties`, {
      params: {
        id: id
      }
    }).map(res => res.json());
  }

  getBuyerRequests(id: string): Observable<any> {
    return this.http.get(`${this.API}/buyer-requests`, {
      params: {
        user_id: id
      }
    }).map(res => res.json())
    .map(res => {
      res.forEach(req => {
        req.date = new Date(parseInt(req.date));
      });
      return res;
    });
  }

  updateProfile(body: any): Observable<UserModel> {
    return this.http.patch(`${this.API}/users/me`, body)
    .map(res => res.json());
  }

  changePassword(body: any): Observable<any> {
    return this.http.post(`${this.API}/users/change-password`, body)
    .map(res => res.json());
  }

  createProperty(body: PropertyModel): Observable<PropertyModel> {
    return this.http.post(`${this.API}/properties`, body)
    .map(res => res.json());
  }

  updateProperty(body: PropertyModel): Observable<PropertyModel> {
    return this.http.patch(`${this.API}/properties`, body)
    .map(res => res.json());
  }

  createBuyerRequest(body: any): Observable<any> {
    return this.http.post(`${this.API}/buyer-requests`, body)
    .map(res => res.json());
  }
}
