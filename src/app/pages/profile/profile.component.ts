import { Component, ViewChild, ElementRef, Renderer } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ProfileService } from "./profile.service";
import { UserService, UploadService, AuthService } from "../../services";
import Swal from "sweetalert2";

@Component({
  selector: "app-profile",
  templateUrl: "profile.html"
})
export class ProfileComponent {
  form: FormGroup;
  pForm: FormGroup;
  perror: string = "";
  user: any = {};
  isFileSelect: boolean = false;
  userProfileImg: string = "";
  @ViewChild("profilePhoto") profilePhoto: ElementRef;
  @ViewChild("photoHover") photoHover: ElementRef;
  constructor(
    private service: ProfileService,
    private fb: FormBuilder,
    private userService: UserService,
    private renderer: Renderer,
    private auth: AuthService,
    private router: Router,
    private uploadService: UploadService
  ) {
    this.userService.fetchAuthUser(true).subscribe(user => {
      this.user = user;
      this.userProfileImg = user.profile_image;
      this.initForm();
    });
    this.pForm = this.fb.group({
      password: ["", Validators.required],
      new_password: ["", Validators.required],
      cnew_password: ["", Validators.required]
    })
  }

  initForm(): void {
    this.form = this.fb.group({
      firstname: [this.user.firstname, Validators.required],
      lastname: [this.user.lastname, Validators.required],
      brokerage: [this.user.brokerage, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      phone: [this.user.phone, Validators.required],
      profile_image: [this.user.profile_image, Validators.required],
      address: ["test", Validators.required],
      about: [this.user.about]
    });
  }

  saveProfile(): void {
    if (!this.form.valid) {
      Swal("Error", "Please fill form correctly", "error");
      return;
    }
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.service.updateProfile(req).subscribe(res => {
      Swal("Success", "Information saved successfully!", "success");
    });
  }

  showUpdatePhoto(): void {
    this.renderer.invokeElementMethod(this.profilePhoto.nativeElement, "click");
  }

  onFileSelect(): void {
    this.isFileSelect = this.profilePhoto.nativeElement.files.length > 0;
    if (this.isFileSelect) {
      let reader: any = new FileReader();
      reader.readAsDataURL(this.profilePhoto.nativeElement.files[0]);
      reader.onload = (e) => {
        this.userProfileImg = e.target.result;
      };
    }
  }

  updateProfileImage(): void {
    if (this.profilePhoto.nativeElement.files.length > 0) {
      let file = this.profilePhoto.nativeElement.files[0];
      this.uploadService.upload(file).subscribe(res => {
        this.service.updateProfile({
          profile_image: `${this.auth.getAPI()}${res.path}`
        }).subscribe(res => {
          Swal("Success", "Profile picture updated successfully!", "success");
          this.profilePhoto.nativeElement.value = "";
          this.onFileSelect();
        });
        this.user.profile_image = `${this.auth.getAPI()}${res.path}`;
        this.form.controls["profile_image"] = this.user.profile_image;
      });
    }
  }

  updatePassword(): void {
    if (!this.pForm.valid) return;
    if (this.pForm.controls["new_password"].value === this.pForm.controls["cnew_password"].value) {
      this.service.changePassword({
        password: this.pForm.controls["password"].value,
        new_password: this.pForm.controls["new_password"].value
      }).subscribe(res => {
        this.perror = res.msg;
        Swal("Success", "Password updated successfully!", "success");
        for (let key in this.pForm.controls) {
          this.pForm.controls[key].setValue("");
        }
      }, err => {
        let res = JSON.parse(err._body);
        Swal("Error", res.msg, "error");
      });
    }
  }

  onImageUpdated(img): void {
    let ele: any = this.photoHover.nativeElement;
    ele.style.width = `${img.width}px`;
    ele.style.height = `${img.height}px`;
  }

  logout(): boolean {
    this.auth.logout();
    this.router.navigate(["login"]);
    return false;
  }
}
