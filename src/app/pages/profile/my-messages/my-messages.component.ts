import { Component, ViewChild, ElementRef, Renderer } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageModel, UserModel } from "../../../models";
import { UserService, AuthService } from "../../../services";
// import { DialogService } from "../../../services/dialog.service";
import { MessageService } from "../../message-board";

@Component({
  selector: "app-my-messages",
  templateUrl: "my-messages.html"
})
export class MyMessagesComponent {
  form: FormGroup;
  messages: MessageModel[] = [];
  user: UserModel = null;
  allMessages: MessageModel[] = [];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  EditMsg = { title: '', msg: '', id: '' };
  editId: string = '';
  showWriteMsg: boolean = true;
  @ViewChild("collpse") collpse: ElementRef;
  constructor(
    private service: MessageService,
    private fb: FormBuilder,
    private userService: UserService,
    private auth: AuthService,
    private router: Router,
    private render: Renderer,
    // public dialog: DialogService
  ) {
    this.userService.fetchAuthUser().subscribe(user => {
      this.user = user;
      this.initForm(user);
      this.getMessages();
    });

  }

  onDelete(msg) {
    if (msg) {
      this.service.remove(msg).subscribe(res => {
        this.getMessages();
      });
    }
  }
  noJohn: any
  abc: any
  xyz: any
  onEdit(msg) {
    // console.log(this.allMessages)
    // this.abc = msg._id
    // this.noJohn = this.allMessages.filter(el => el._id !== msg._id);
    // this.xyz = this.noJohn[0]._id
    this.form.patchValue({
      title: msg.title,
      message: msg.message,
      id: msg._id
    })
    this.editId = msg._id;
    this.showWriteMsg = !this.showWriteMsg
  }

  onEditMessage(): void {
    // debugger
    if (!this.form.valid) return;
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.service.edit(req).subscribe(res => {
      // this.render.invokeElementMethod(this.collpse.nativeElement, "click");
      this.editId = '';
      this.getMessages();
      // this.form.reset();
    });
  }

  getMessages(): void {
    this.service.get(this.user._id).subscribe(msgs => {
      this.messages = msgs;
      this.allMessages = msgs;
      console.log("this.messages", this.messages)
      this.resetPages();
      this.changePage(1);
    });
  }


  initForm(user: UserModel): void {
    this.form = this.fb.group({
      firstname: [user.firstname, Validators.required],
      lastname: [user.lastname, Validators.required],
      email: [user.email, Validators.required],
      phone: [user.phone, Validators.required],
      profile_image: [user.profile_image, Validators.required],
      title: ["", Validators.required],
      message: ["", Validators.required],
      date: [new Date().getTime(), Validators.required],
      id: ''
    });
  }

  writeMsg() {
    // this.form.reset();
    this.form.patchValue({
      title: "",
      message: " ",
      id: " "
    })
    this.showWriteMsg = true;
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.allMessages.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.messages = this.allMessages.slice().splice(this.pageStart, this.pageLimit);
    });
  }

  onCreateMessage(): void {
    if (!this.form.valid) return;
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.service.create(req).subscribe(res => {
      this.render.invokeElementMethod(this.collpse.nativeElement, "click");
      this.getMessages();
    });
  }



  trimEmail(text) {
    let chars = text.length;
    if (chars > 16) {
      let trimmedSummary = text.substring(0, 16) + "...";
      return trimmedSummary;
    }
    else {
      return text
    }
  }
  trimPhone(text) {
    let chars = text.length;
    if (chars > 21) {
      let trimmedSummary = text.substring(0, 21) + "...";
      return trimmedSummary;
    }
    else {
      return text
    }
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.allMessages.length / this.pageLimit)) {
      return `${this.allMessages.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }

  logout(): boolean {
    this.auth.logout();
    this.router.navigate(["login"]);
    return false;
  }
}
