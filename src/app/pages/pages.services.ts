import { PaymentService } from "./payment";
import { ProfileService } from "./profile";
import { ListingService } from "./listing";
import { AgentService } from "./agents";
import { TourService } from "./tour";
import { MessageService } from "./message-board";

export const PagesServices = [
  PaymentService,
  MessageService,
  ProfileService,
  ListingService,
  TourService,
  AgentService
];
