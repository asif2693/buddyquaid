import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import { UserModel, PaymentMethodModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class PaymentService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/users`;
  }

  payStripe(body: PaymentMethodModel): Observable<UserModel> {
    return this.http.post(`${this.API}/payStripe`, body)
    .map(res => res.json());
  }

  verifyUserEmail(link: string): Observable<any> {
    return this.http.post(`${this.auth.getAPI()}/auth/confirm`, {
      verify_link: link
    }).map(res => res.json());
  }
}
