import { Component, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators, ValidationErrors } from "@angular/forms";
import { PaymentService } from "./payment.service";
import { AuthService, UserService } from "./../../services";
import { environment } from "../../../environments/environment";
import Swal from "sweetalert2";

@Component({
  selector: "app-payment",
  templateUrl: "payment.html"
})
export class PaymentComponent implements OnDestroy {
  form: FormGroup;
  sub: any;
  processing: boolean = false;
  constructor(
    private service: PaymentService,
    private fb: FormBuilder,
    private router: Router,
    private auth: AuthService,
    private userService: UserService,
    private activated: ActivatedRoute
  ) {
    this.initForm();
    this.sub = this.activated.params.subscribe(params => {
      if (params.id) {
        this.verifyEmail(params.id);
      }
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      email: ["", Validators.required],
      method: ["visa", Validators.required],
      number: ["", [Validators.required, Validators.pattern('[0-9]+')]],
      exp_month: ["01", Validators.required],
      exp_year: ["2018", Validators.required],
      csv: ["", [Validators.required, Validators.pattern('[0-9]+')]],
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      address: ["", Validators.required],
      address2: [""],
      city: ["", Validators.required],
      state: ["", Validators.required],
      zip: ["", Validators.required]
    });
  }

  verifyEmail(link): void {
    this.service.verifyUserEmail(link).subscribe(res => {
      if (res.token) {
        this.auth.setAuthToken(res.token);
        this.auth.setAuthUser(res);
        this.form.controls["email"].setValue(res.email);
        this.form.controls["firstname"].setValue(res.firstname);
        this.form.controls["lastname"].setValue(res.lastname);
      }
    }, err => {
      this.router.navigate(["/"]);
    });
  }

  onPayStripe(): void {
    if (!this.form.valid) {
      this.getFormValidationErrors();
      return;
    }
    Stripe.setPublishableKey(environment.stripeKey);
    let tokenRequest: any = {
      number: this.form.controls["number"].value,
      cvc: this.form.controls["csv"].value,
      exp_month: this.form.controls["exp_month"].value,
      exp_year: this.form.controls["exp_year"].value
    };
    
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.processing = true;
    setTimeout(() => {
      Stripe.createToken(tokenRequest, (status, response) => {
        if ((response || {}).id && status == 200) {
          req.pay_source = response.id;
          this.service.payStripe(req).subscribe(res => {
            window.dispatchEvent(new Event("onUserRefresh"));
            this.userService.fetchAuthUser(true).subscribe(res => {
              this.processing = false;
              document.location.href = "/";
            });
          });
        } else {
          this.processing = false;
          Swal("Error", "Invalid payment information", "error");
        }
      });
    });
  }

  getFormValidationErrors() {
    let isError = false;
    Object.keys(this.form.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.form.get(key).errors;
      if (controlErrors != null && !isError) {
        // Object.keys(controlErrors).forEach(keyError => {
        //   console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        // });
        isError = true;
        Swal("Error", this.getErrorMsg(key), "error");
      }
    });
    return isError;
  }

  getErrorMsg(key): string {
    let prefix = "Please enter a valid";
    let errors: any = {
      email: `${prefix} Email`,
      method: ["visa", Validators.required],
      number: `${prefix} card number`,
      exp_month: `${prefix} expiry month`,
      exp_year: `${prefix} expiry year`,
      csv: `${prefix} CSV/CVV`,
      firstname: `${prefix} first name`,
      lastname: `${prefix} last name`,
      address: `${prefix} address`,
      address2: [""],
      city: `${prefix} city`,
      state: `${prefix} state`,
      zip: `${prefix} zip`
    };
    return errors[key];
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
