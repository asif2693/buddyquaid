import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[myNumberOnly]'
})
export class NumberOnlyDirective {
  // Allow decimal numbers and negative values
  private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', '-' ];
  private ctrlKey: boolean = false;

  constructor(private el: ElementRef) { }
 
  @HostListener('keydown', [ '$event' ])
  onKeyDown(event: KeyboardEvent) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    if (event.ctrlKey || [91, 93].indexOf(event.keyCode) >= 0) {
      this.ctrlKey = true;
    }
    if (this.ctrlKey && [86].indexOf(event.keyCode) >= 0) {
      return true;
    } else {
      let current: string = this.el.nativeElement.value;
      let next: string = current.concat(event.key);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
    }
  }

  @HostListener('keyup', [ '$event' ])
  onKeyUp(event: KeyboardEvent) {
    if (this.ctrlKey && [91, 93].indexOf(event.keyCode)) {
      this.ctrlKey = false;
    }
  }
}