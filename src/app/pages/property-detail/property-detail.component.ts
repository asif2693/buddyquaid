import { Component, AfterViewInit, OnDestroy } from "@angular/core";
import { DomSanitizer, SafeUrl, SafeHtml } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { ListingService } from "../listing";
import { PropertyModel } from "../../models";
import { UserService } from "../../services";

@Component({
  selector: "app-property-detail",
  templateUrl: "property-detail.html"
})
export class PropertyDetailComponent implements AfterViewInit, OnDestroy {
  subs: any = {};
  property: any = {};
  propUser: any = {};
  showfeatures: string[][] = [];
  relProps: any = [];
  propMap: SafeUrl;
  isVimeoOrYoutube: boolean = false;
  constructor(
    private service: ListingService,
    private userService: UserService,
    private sanitizer: DomSanitizer,
    private activated: ActivatedRoute
  ) {
    this.getPropertyDetails();
  }

  ngAfterViewInit(): void { }

  getPropertyDetails(): void {
    this.subs["params"] = this.activated.params.subscribe(params => {
      if (params.id) {
        this.property ='';
        this.service.getProperties(params).subscribe(prop => {
          this.splitFeatures(prop.features);
          this.userService.fetchUserProfile(prop.user_id).subscribe(user => {
            this.property = prop;
            // this.propMap = this.sanitizer.bypassSecurityTrustResourceUrl(this.property.place_on_map);
            this.propMap = this.property.place_on_map;
            this.propUser = user;
            this.service.getProperties(prop.user_id).subscribe(relProps => {
              this.relProps = relProps.splice(0, 3);
              this.relProps = this.relProps.filter( el => el._id !== prop._id);
              initPropertyCarousel();
            });
          });
        });
      }
    });
  }

  trimAbout(text) {
    let chars = text.length;
    if (chars > 60) {
      let trimmedSummary = text.substring(0, 60) + "...";
      return trimmedSummary;
    }
    else {
      return text
    }
  }

  splitFeatures(features: string[]): void {
    let length = Math.ceil(features.length / 3);
    let temp: any[] = [];
    let start = 0;
    for (let i = 0; i < length; i++) {
      temp[i] = [];
      temp[i].push(features.splice(start, length));
      start = start + length;
    }
    this.showfeatures = temp;
  }

  getSafeUrl(url): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  convertToCommas(value) {
    if (!value) return "";
    return `$${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  }

  ngOnDestroy(): void {
    for (let key in this.subs) {
      this.subs[key].unsubscribe();
    }
  }
}
