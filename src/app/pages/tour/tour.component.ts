import { Component } from "@angular/core";
import { TourService } from "./tour.service";
import { ListingService } from "../listing";
import { UserService } from "../../services";
import { PropertyModel } from "../../models";

@Component({
  selector: "app-tour",
  templateUrl: "tour.html"
})
export class TourComponent {
  tours: any = {
    upcoming: [],
    scheduled: [],
    past: []
  };
  myProperties: PropertyModel[] = [];
  selectedProps: number[] = [];
  tour: any = {};
  curDate: Date = new Date();
  constructor(
    private service: TourService,
    private listingService: ListingService,
    private userService: UserService
  ) {
    this.getTours();
  }

  getTours(): void {
    this.service.getTours().subscribe(tours => {
      this.tours = tours;
    });
    this.userService.fetchAuthUser().subscribe(user => {
      this.listingService.getProperties({
        user_id: user._id
      }).subscribe(props => {
        this.myProperties = props.slice(0, 6);
      });
    });
  }

  isDatePassed(date): boolean {
    return (date - (<any>this.curDate)) < 0;
  }

  onToggleProperty(idx: number): void {
    if (this.selectedProps.indexOf(idx) >= 0) {
      this.selectedProps.splice(this.selectedProps.indexOf(idx), 1);
    } else {
      this.selectedProps.push(idx)
    }
  }

  onSubmitProperty(tour) {
    this.tour = tour;
  }

  onSubmit(): void {
    let props = [];
    let tour = this.tour;
    if (tour.property) {
      props = tour.property || [];
    }
    this.selectedProps.forEach(key => {
      let prop = this.myProperties[key];
      let exist = props.filter(p => p._id === prop._id).length > 0;
      if (!exist) {
        props.push(prop);
      }
    });
    tour.property = props;
    tour.submission_date = tour.submission_date.getTime();
    tour.exp_date = tour.exp_date.getTime();
    tour.tour_date = tour.tour_date.getTime();
    this.service.update(tour).subscribe(res => {
      this.tour = res;
    });
  }

  isSelectedProp(idx): boolean {
    return this.selectedProps.indexOf(idx) >= 0;
  }

  isAlreadyIn(_id: string): boolean {
    if (!this.tour.property) return false;
    return this.tour.property.filter(p => p._id === _id).length > 0;
  }
}
