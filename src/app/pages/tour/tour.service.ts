import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import { UserModel, PaymentMethodModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class TourService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/tours`;
  }

  getTours(params?: any, noSplit?: boolean): Observable<any> {
    params = params ? params : {};
    return this.http.get(this.API, {
      params: params
    })
    .map(res => res.json())
    .map(res => {
      res.forEach(t => {
        t.submission_date = new Date(parseInt(t.submission_date));
        t.exp_date = new Date(parseInt(t.exp_date));
        t.tour_date = new Date(parseInt(t.tour_date));
      });
      if (!noSplit) {
        res.sort((a, b) => {
          return b.tour_date - a.tour_date;
        });
        let data: any = {};
        data.scheduled = res.filter(t => {
          return t.is_scheduled;
        });
        data.upcoming = res.filter(t => {
          return !t.is_scheduled && t.submission_date > new Date();
        });
        data.past = res.filter(t => {
          return t.tour_date < new Date();
        });
        return data;
      } else {
        return res;
      }
    });
  }

  getProperties(params: any): Observable<any> {
    params = params ? params : {};
    return this.http.get(`${this.API}/getTourProperties`, params)
    .map(res => res.json());
  }

  update(body: TourProperty): Observable<any> {
    return this.http.patch(this.API, body)
    .map(res => res.json());
  }
}

export interface TourProperty {
  tour_id: string;
  property_id: string;
  property: any;
}
