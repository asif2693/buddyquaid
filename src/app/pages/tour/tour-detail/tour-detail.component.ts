import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TourService } from "../tour.service";
import { UserService } from "../../../services";
import { ListingService } from "../../listing";

@Component({
  selector: "app-tour-detail",
  templateUrl: "tour-detail.html"
})
export class TourDetailComponent implements OnDestroy {
  subs: any = {};
  myProperties: any[] = [];
  tour: any = {};
  selectedProps: any[] = [];
  curDate: Date = new Date();
  constructor(
    private tourService: TourService,
    private listingServce: ListingService,
    private userService: UserService,
    private activated: ActivatedRoute
  ) {
    this.getInfo();
  }

  getInfo(): void {
    this.subs["params"] = this.activated.params.subscribe(params => {
      if (params.id) {
        this.tourService.getTours({
          id: params.id
        }, true).subscribe(tour => {
          this.userService.fetchAuthUser().subscribe(user => {
            this.listingServce.getProperties({
              user_id: user._id
            }).subscribe(uprops => {
              this.tour = tour.length > 0 ? tour[0] : {};
              this.myProperties = uprops.slice(0, 6);
              this.selectedProps = this.tour.property ? this.tour.property.slice() : [];
            });
          });
        });
      }
    });
  }

  onToggleProperty(prop): void {
    let found = -1;
    this.selectedProps.forEach((p, i) => {
      if (p._id === prop._id) {
        found = i;
      }
    });
    if (found >= 0) {
      this.selectedProps.splice(found, 1);
    } else {
      this.selectedProps.push(prop);
    }
  }

  isDatePassed(date): boolean {
    return (date - (<any>this.curDate)) < 0;
  }

  isSelectedProp(prop): boolean {
    return this.selectedProps.filter(p => p._id === prop._id).length > 0;
  }

  onSubmit(): void {
    let props = [];
    let tour = this.tour;
    tour.property = this.selectedProps || [];
    tour.submission_date = tour.submission_date.getTime();
    tour.exp_date = tour.exp_date.getTime();
    tour.tour_date = tour.tour_date.getTime();
    this.tourService.update(tour).subscribe(res => {
      this.tour = res;
    });
  }

  onPrint(): void {
    window.print();
  }

  ngOnDestroy(): void {
    for (let key in this.subs) {
      this.subs[key].unsubscribe();
    }
  }
}
