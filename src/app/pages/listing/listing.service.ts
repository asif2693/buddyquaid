import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel, PropertyModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class ListingService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/properties`;
  }

  getProperties(params?: any): Observable<any> {
    // let url = user_id ? `${this.API}/properties/${user_id}` : `${this.API}/properties`;
    params = params ? params : {};
    return this.http.get(this.API, {
      params: params
    })
    .map(res => res.json())
    .map(res => {
      if (res instanceof Array) {
        res.forEach(p => {
          p.date = new Date(parseInt(p.date));
          if (p.removed_date) {
            p.removed_date = new Date(parseInt(p.removed_date));
          }
        });
        res.sort((a, b) => b.date - a.date);
      } else {
        res.date = new Date(parseInt(res.date));
      }
      return res;
    });
  }

  getMaxRange(params: any): Observable<any> {
    return this.http.get(`${this.API}/getMax`, { params: params })
    .map(res => res.json());
  }
}
