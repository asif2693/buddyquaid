import { Component, ViewChild, ElementRef, Renderer } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MessageModel } from "../../models";
import { UserService } from "../../services";
import { MessageService } from "./message.service";

@Component({
  selector: "app-message-board",
  templateUrl: "message-board.html"
})
export class MessageBoardComponent {
  form: FormGroup;
  messages: MessageModel[] = [];
  allMessages: MessageModel[] = [];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  @ViewChild("collpse") collpse: ElementRef;
  constructor(
    private service: MessageService,
    private fb: FormBuilder,
    private userService: UserService,
    private render: Renderer
  ) {
    this.initForm();
    this.getMessages();
  }

  getMessages(): void {
    this.service.get().subscribe(msgs => {
      this.messages = msgs;
      this.allMessages = msgs;
      this.resetPages();
      this.changePage(1);
    });
  }

  initForm(): void {
    this.userService.fetchAuthUser().subscribe(user => {
      this.form = this.fb.group({
        firstname: [user.firstname, Validators.required],
        lastname: [user.lastname, Validators.required],
        email: [user.email, Validators.required],
        phone: [user.phone, Validators.required],
        profile_image: [user.profile_image, Validators.required],
        title: ["", Validators.required],
        message: ["", Validators.required],
        date: [new Date().getTime(), Validators.required]
      });
    });
  }

  onCreateMessage(): void {
    if (!this.form.valid) return;
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.service.create(req).subscribe(res => {
      this.render.invokeElementMethod(this.collpse.nativeElement, "click");
      this.getMessages();
      this.form.reset();
    });
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.allMessages.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.messages = this.allMessages.slice().splice(this.pageStart, this.pageLimit);
    });
  }

  trimEmail(text) {
    let chars = text.length;
    if (chars > 17) {
      let trimmedSummary = text.substring(0, 17) + "...";
      return trimmedSummary;
    }
    else {
      return text
    }
  }
  trimPhone(text) {
    let chars = text.length;
    if (chars > 25) {
      let trimmedSummary = text.substring(0, 25) + "...";
      return trimmedSummary;
    }
    else {
      return text
    }
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.allMessages.length / this.pageLimit)) {
      return `${this.allMessages.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }
}
