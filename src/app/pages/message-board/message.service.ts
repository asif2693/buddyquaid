import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import { MessageModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class MessageService {
  private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/messages`;
  }

  remove(msg: any): Observable<any> {
    return this.http.delete(`${this.API}/${msg._id}`)
      .map(res => res.json());
  }

  create(body: MessageModel): Observable<MessageModel> {
    return this.http.post(this.API, body)
    .map(res => res.json());
  }

  edit(msg: any): Observable<any> {
	  debugger
    return this.http.patch(this.API,msg)
    .map(res => res.json());
  }

  get(user_id?: string): Observable<MessageModel[]> {
    return this.http.get(user_id ? `${this.API}/${user_id}` : `${this.API}`)
    .map(res => res.json())
    .map(res => {
      res.forEach(m => {
        m.date = new Date(parseInt(m.date));
      });
      res.sort((a, b) => {
        return b.date - a.date;
      });
      return res;
    });
  }
}
