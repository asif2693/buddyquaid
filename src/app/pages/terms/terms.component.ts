import { Component } from "@angular/core";
import { AuthService, MiscService } from "../../services";

@Component({
  selector: "app-terms",
  templateUrl: "terms.html"
})
export class TermsComponent {
  isLogin: boolean;
  terms: any = "";
  constructor(private auth: AuthService, private misc: MiscService) {
    this.isLogin = this.auth.isLogin();
    this.getTerms();
  }

  getTerms(): void {
    this.misc.getPrivacy().subscribe(res => {
      res.forEach(i => {
        if (i.type === "terms") {
          this.terms = i.text;
        }
      });
    });
  }
}
