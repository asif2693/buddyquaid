import { Component, AfterViewInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ListingService } from "../../listing";
import { PropertyModel } from "../../../models";
import { UserService } from "../../../services";

@Component({
  selector: "app-agent-detail",
  templateUrl: "agent-detail.html"
})
export class AgentDetailComponent {
  subs: any = {};
  user: any = {};
  properties: PropertyModel[] = [];
  allProperties: PropertyModel[];
  pageStart: number = 0;
  pageLimit: number = 12;
  totalPages: any[] = [];
  currentPage: number = 1;
  tempProperties: PropertyModel[] = [];
  reqCompleted: boolean = false;
  constructor(
    private service: ListingService,
    private userService: UserService,
    private activated: ActivatedRoute
  ) {
    this.getPropertyDetails();
  }

  ngAfterViewInit(): void {
    initPropertyCarousel();
  }

  getPropertyDetails(): void {
    this.subs["params"] = this.activated.params.subscribe(params => {
      if (params.id) {
        this.service.getProperties({
          user_id: params.id
        }).subscribe(props => {
          this.properties = props;
          this.reqCompleted = true;
          this.allProperties = props.slice();
          this.resetPages();
          this.changePage(1);
        });
        this.userService.fetchUserProfile(params.id).subscribe(user => {
          this.user = user;
        });
      }
    });
  }

  resetPages(): void {
    setTimeout(() => {
      this.totalPages = [];
      let temp = [];
      for (let i = 0; i < Math.ceil(this.properties.length / this.pageLimit); i++) {
        temp.push(i + 1);
      }
      this.totalPages = Object.values(temp);
      this.pageStart = 0;
    })
  }

  changePage(page: number): void {
    setTimeout(() => {
      this.pageStart = (this.pageLimit * page) - this.pageLimit;
      this.currentPage = page;
      this.tempProperties = this.properties.slice().splice(this.pageStart, this.pageLimit);
    });
  }

  getCurrentRange(): string {
    if (this.currentPage === Math.ceil(this.properties.length / this.pageLimit)) {
      return `${this.properties.length}`
    } else {
      return `${this.pageStart + this.pageLimit}`;
    }
  }

  convertToCommas(value) {
    if (!value) return "";
    return `$${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  }

  ngOnDestroy(): void {
    for (let key in this.subs) {
      this.subs[key].unsubscribe();
    }
  }
}
