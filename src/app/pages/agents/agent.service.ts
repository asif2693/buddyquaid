import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel, PropertyModel } from "../../models";
import { AuthService } from "../../services";

@Injectable()
export class AgentService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/users`;
  }

  getAgents(params?: any): Observable<UserModel[]> {
    // let url = user_id ? `${this.API}/properties/${user_id}` : `${this.API}/properties`;
    params = params ? params : {};
    return this.http.get(this.API, {
      params: params
    })
    .map(res => res.json())
    .map(res => {
      res.forEach(p => {
        p.date = new Date(parseInt(p.date));
      });
      res.sort((a, b) => b.date - a.date);
      return res;
    });
  }
}
