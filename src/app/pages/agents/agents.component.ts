import { Component } from "@angular/core";
import { AgentService } from "./agent.service";
import { UserModel } from "../../models";

@Component({
  selector: "app-agents",
  templateUrl: "agents.html"
})
export class AgentsComponent {
  agents: UserModel[] = [];
  allAgents: UserModel[] = [];
  searchWord: string = "";
  sortKey: string = "All";
  alphabets: string[] = ["All", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
  constructor(
    private service: AgentService
  ) {
    let temp = this.alphabets[1].split("");
    this.alphabets.splice(1, 1);
    this.alphabets = this.alphabets.concat(temp);
    this.getAgents();
  }

  getAgents(): void {
    this.service.getAgents({
      user_type: "agent",
      isPaymentAdded: "true"
    }).subscribe(res => {
      this.agents = res;
      this.allAgents = res.slice();
    });
  }

  sortByAlphabet(key?: string): UserModel[] {
    this.sortKey = key;
    let agents = this.allAgents.slice();
    if (key != "All") {
      agents = this.allAgents.slice().filter(agent => {
        return agent.firstname.toLowerCase().startsWith(this.sortKey.toLowerCase());
      });
    }
    this.agents = agents;
    return agents;
  }

  searchAgent(): void {
    setTimeout(() => {
      let agents = this.sortByAlphabet(this.sortKey);
      this.agents = agents.slice().filter(agent => {
        return agent.firstname.indexOf(this.searchWord) >= 0 || 
        agent.lastname.indexOf(this.searchWord) >= 0 || 
        agent.email.indexOf(this.searchWord) >= 0 || 
        agent.phone.indexOf(this.searchWord) >= 0;
      });
    });
  }
}
