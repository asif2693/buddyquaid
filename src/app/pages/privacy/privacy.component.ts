import { Component } from "@angular/core";
import { AuthService, MiscService } from "../../services";

@Component({
  selector: "app-privacy",
  templateUrl: "privacy.html"
})
export class PrivacyComponent {
  isLogin: boolean;
  privacy: any[] = [];
  constructor(private auth: AuthService, private misc: MiscService) {
    this.isLogin = this.auth.isLogin();
    this.getPrivacy();
  }

  getPrivacy(): void {
    this.misc.getPrivacy().subscribe(res => {
      res.forEach(i => {
        if (i.type === "privacy") {
          this.privacy = i.text;
        }
      });
    });
  }
}
