import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService, MiscService } from "../../services";
import Swal from "sweetalert2";

@Component({
  selector: "app-contact-us",
  templateUrl: "contact-us.html"
})
export class ContactUsComponent {
  isLogin: boolean;
  form: FormGroup;
  constructor(
    private auth: AuthService,
    private miscService: MiscService,
    private fb: FormBuilder
  ) {
    this.isLogin = this.auth.isLogin();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      s_email: true,
      name: ["", Validators.required],
      email: ["", Validators.required],
      phone: ["", Validators.required],
      subject: ["", Validators.required],
      msg: ["", Validators.required],
    });
  }

  sendMessage(): void {
    if (!this.form.valid) {
      Swal("Error", "Please fill the form correctly", "error");
    }
    let req: any = {};
    for (let key in this.form.controls) {
      req[key] = this.form.controls[key].value;
    }
    this.miscService.sendContactMsg(req);
    this.initForm();
    Swal("Success", "Message sent successfully!", "success");
  }
}
