import {NgModule, Pipe, PipeTransform} from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SectionsModule } from "./sections";

import { ListingComponent } from "./listing";
import { PaymentComponent, NumberOnlyDirective } from "./payment";
import { FaqsComponent } from "./faqs";
import { TermsComponent } from "./terms";
import { ContactUsComponent } from "./contact-us";
import { LeaseComponent } from "./lease";
import { PrivacyComponent } from "./privacy";
import { TourComponent, TourDetailComponent } from "./tour";
import { HistoryComponent } from "./history";
import { PropertyDetailComponent } from "./property-detail";
import { MessageBoardComponent } from "./message-board";
import { AgentsComponent, AgentDetailComponent } from "./agents";
import { ProfileComponent, MyMessagesComponent, SubmitPropertyComponent, DndDirective, MyPropertiesComponent, BuyersRequestsComponent  } from "./profile";
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
@NgModule({
  imports: [
    CommonModule,
    SectionsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    ListingComponent,
    PaymentComponent,
    FaqsComponent,
    TermsComponent,
    ContactUsComponent,
    LeaseComponent,
    AgentsComponent,
    AgentDetailComponent,
    MessageBoardComponent,
    HistoryComponent,
    TourComponent,
    TourDetailComponent,
    ProfileComponent,
    MyMessagesComponent,
    SubmitPropertyComponent,
    MyPropertiesComponent,
    BuyersRequestsComponent,
    PropertyDetailComponent,
    NumberOnlyDirective,
    PrivacyComponent,
    DndDirective,
    SafePipe
  ],
  exports: [],
  providers: []
})
export class PagesModule { }
