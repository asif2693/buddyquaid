import { Component } from "@angular/core";

@Component({
  selector: "app-header",
  template: `
    <header>
      <nav class="navbar navbar-expand-xl navbar-light ">
        <div class="container-fluid">
          <a class="navbar-brand" routerLink="/"><img src="assets/images/svg/logo.svg" alt="BuddyQuaid"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupported">
            <ul class="navbar-nav">
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/">Listings</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/lease">Lease</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/message-board">Message board</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/tour">Tour</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/history">History</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/profile">My Account</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  `
})
export class HeaderComponent { }
