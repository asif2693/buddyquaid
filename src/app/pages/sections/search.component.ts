import { Component, ViewChild, ElementRef, EventEmitter, Output, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SettingService, Setting } from "../../services";
import { ListingService } from "../listing";
import { StatusValsActive } from "../profile/submit-property";

@Component({
  selector: "app-search-bar",
  template: `
    <section>
      <div class="container">
        <div class="row">
          <div class="col text-center">
            <h1 class="uppercase mt-5 mb-4">Advanced Search</h1>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="search pt-4 pb-4 pl-4 pr-4">
          <form class="" [formGroup]="form" *ngIf="form">
            <div class="row  ">
              <div class="col-md-6 col-lg-4 col-xl-3  col-sm-6">
                <div class="form-group">
                  <div class="intro">
                    <select class="form-control" [formControl]="form.controls.city">
                      <option value="">City</option>
                      <option *ngFor="let key of settings.cities; let i = idx">{{key.title}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3 col-sm-6">
                <div class="form-group">
                  <div class="intro">
                    <select class="form-control" [formControl]="form.controls.mls">
                      <option value="">MLS Area</option>
                      <option *ngFor="let key of settings.mls; let i = idx">{{key.title}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3 col-sm-6">
                <div class="form-group">
                  <div class="intro">
                    <select class="form-control" [formControl]="form.controls.prop_type">
                      <option value="">Property Type</option>
                      <option *ngFor="let key of settings.properties; let i = idx">{{key.title}}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3 col-sm-6">
                <div class="row search-2">
                  <div class="col-6 col-sm-6">
                    <div class="form-group">
                      <div class="intro">
                        <select class="form-control" [formControl]="form.controls.bedrooms">
                          <option value="">Min Beds</option>
                          <option *ngFor="let key of [1,2,3,4,5,6,7,8]">{{key}}</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-6 col-sm-6">
                    <div class="form-group">
                      <div class="intro">
                        <select class="form-control" [formControl]="form.controls.bathrooms">
                          <option value="">Min Baths</option>
                          <option *ngFor="let key of [1,2,3,4,5,6,7,8]">{{key}}</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-4 ">
                <div class="single-query-slider">
                  <label>Size Range(Sq Ft) :</label>
                  <div class="price text-right">
                    <span></span>
                    <div #sizeMin class="leftLabel"></div>
                    <span>to</span>
                    <div #sizeMax class="rightLabel"></div>
                  </div>
                  <div #sizeDiv data-range_min="0" data-range_max="9007199254740991" data-cur_min="0" data-cur_max="9007199254740991" class="nstSlider">
                    <div class="bar"></div>
                    <div class="leftGrip"></div>
                    <div class="rightGrip"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-4  ">
                <div class="single-query-slider">
                  <label>Price Range:</label>
                  <div class="price text-right">
                    <span>$</span>
                    <div #priceMin class="leftLabel"></div>
                    <span>to $</span>
                    <div #priceMax class="rightLabel"></div>
                  </div>
                  <div #priceDiv data-range_min="0" data-range_max="9007199254740991" data-cur_min="0" data-cur_max="9007199254740991" class="nstSlider">
                    <div class="bar"></div>
                    <div class="leftGrip"></div>
                    <div class="rightGrip"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-4">
                <div class="form-group">
                  <div class="intro">
                    <select class="form-control" [formControl]="form.controls.sort_by">
                      <option value="">Sort by price</option>
                      <option value="asc">Ascending</option>
                      <option value="des">Descending</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-lg-4 col-xl-3   text-right form-group">
                <button type="button" (click)="onSearch()" class="btn-brown w-100">Search</button>
              </div>
              <div class="col-md-3 col-lg-4 col-xl-3   text-right form-group">
                <button type="button" (click)="onClear()" class="btn-brown w-100">Reset</button>
              </div>
              <div class="col-md-4 col-lg-3 group-button-search">
                <a data-toggle="collapse" href=".search-propertie-filters" class="more-filter">
                  <i class="fa fa-plus text-1" aria-hidden="true"></i><i class="fa fa-minus text-2 hide" aria-hidden="true"></i>
                  <div class="text-1">Show more search options</div>
                  <div class="text-2 hide">less more search options</div>
                </a>
              </div>
            </div>
            <div class="search-propertie-filters collapse mt-4">
              <div class="row">
                <ng-template ngFor [ngForOf]="settings.features" let-feature>
                  <div class="col-md-4 col-lg-3  col-sm-6">
                    <div class="search-form-group ">
                      <input type="checkbox" [checked]="isFeatureAvailable(feature.title)" (change)="toggleFeature(feature.title)" name="check-box" />
                      <span>{{feature.title}}</span>
                    </div>
                  </div>
                </ng-template>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  `
})
export class SearchBarComponent implements AfterViewInit {
  @ViewChild("priceMin") priceMin: ElementRef;
  @ViewChild("priceMax") priceMax: ElementRef;
  @ViewChild("sizeMin") sizeMin: ElementRef;
  @ViewChild("sizeMax") sizeMax: ElementRef;
  @ViewChild("priceDiv") priceDiv: ElementRef;
  @ViewChild("sizeDiv") sizeDiv: ElementRef;
  settings: any = {
    cities: [],
    features: [],
    mls: [],
    properties: []
  };
  hideShowOptions: boolean = true;
  form: FormGroup;
  features: string[] = [];
  @Output() onSearchApplied: EventEmitter<any> = new EventEmitter();
  @Output() onClearApplied: EventEmitter<any> = new EventEmitter();
  constructor(
    private settingService: SettingService,
    private listingService: ListingService,
    private fb: FormBuilder
  ) {
    this.getSettings();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      city: [""],
      mls: [""],
      prop_type: [""],
      bedrooms: [""],
      bathrooms: [""],
      sort_by: [""]
    });
  }

  ngAfterViewInit(): void {
    this.listingService.getMaxRange({
      statusVals: StatusValsActive.join(",")
    }).subscribe(res => {
      this.priceDiv.nativeElement.setAttribute("data-range_max", 200000000);
      this.sizeDiv.nativeElement.setAttribute("data-range_max", 15000);
      initNSTSlider();
    });
  }

  getSettings(): void {
    this.settingService.getSettings().subscribe(res => {
      this.settings.cities = res.filter(s => s.type === "city");
      this.settings.features = res.filter(s => s.type === "feature");
      this.settings.mls = res.filter(s => s.type === "mls");
      this.settings.properties = res.filter(s => s.type === "property");
    });
  }

  toggleFeature(name): void {
    let feat = this.features.filter(f => f === name);
    if (feat.length > 0) {
      this.features.splice(this.features.indexOf(name), 1);
    } else {
      this.features.push(name);
    }
  }

  onSearch(): void {
    let searchData: any = {
      price_min: parseInt(this.priceMin.nativeElement.textContent),
      price_max: parseInt(this.priceMax.nativeElement.textContent),
      size_prefix_min: parseInt(this.sizeMin.nativeElement.textContent),
      size_prefix_max: parseInt(this.sizeMax.nativeElement.textContent),
      features: this.features
    };
    for (let key in this.form.controls) {
      if (this.form.controls[key].value) {
        searchData[key] = this.form.controls[key].value;
      }
    }
    this.onSearchApplied.emit(searchData);
  }

  onClear(): void {
    resetNSTSlider();
    this.onClearApplied.emit(true);
  }

  isFeatureAvailable(name): boolean {
    return this.features.indexOf(name) >= 0;
  }

  toggleHideShowOptions(): void {
    this.hideShowOptions = !this.hideShowOptions;
  }
}
