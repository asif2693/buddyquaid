import { Component } from "@angular/core";

@Component({
  selector: "app-footer",
  template: `
  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg">
          <p class="text-center text-lg-left">Copy Right @ 2018 buddyquaid. All Right Reserved </p>
        </div>
        <div class="col-lg text-center text-lg-right">
          <ul class="">
            <li><a routerLink="/agents">Agents</a></li>
            <li><a routerLink="/faqs">Faqs</a></li>
            <li><a routerLink="/terms">Terms</a></li>
            <li><a routerLink="/privacy">Privacy</a></li>
            <li><a routerLink="/contact-us">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  `
})
export class FooterComponent { }
