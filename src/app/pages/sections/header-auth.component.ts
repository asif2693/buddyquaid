import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-auth-header",
  template: `
    <header>
      <nav class="navbar navbar-expand-sm navbar-light" [class.bg-dark]="bgDark">
        <div class="container-fluid">
          <a class="navbar-brand" routerLink="/"><img src="assets/images/svg/logo.svg" alt="BuddyQuaid"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupported">
            <ul class="navbar-nav">
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/faqs">Faqs</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/terms">Terms</a></li>
              <li><a [routerLinkActiveOptions]="{exact: true}" routerLinkActive="act" routerLink="/contact-us">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  `
})
export class AuthHeaderComponent {
  bgDark: boolean = false;
  constructor(private router: Router) {
    this.bgDark = ["/login", "/signup"].indexOf(this.router.url) >= 0;
  }
}
