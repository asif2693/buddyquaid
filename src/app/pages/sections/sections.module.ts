import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { HeaderComponent } from "./header.component";
import { AuthHeaderComponent } from "./header-auth.component";
import { FooterComponent } from "./footer.component";
import { SearchBarComponent } from "./search.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    AuthHeaderComponent,
    SearchBarComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    AuthHeaderComponent,
    SearchBarComponent
  ],
  providers: []
})
export class SectionsModule { }
