import { Injectable, Injector } from "@angular/core";
import { Http, XHRBackend, ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, Headers, Request } from "@angular/http";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { JwtHelper } from "angular2-jwt";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/do";
import "rxjs/add/operator/finally";
import "rxjs/add/observable/throw";

@Injectable()
export class HttpInterceptor extends Http {
  private auth: AuthService;
  private helper: JwtHelper = new JwtHelper();
  constructor(
    backend: ConnectionBackend,
    defaultOptions: RequestOptions,
    injector: Injector) {
    super(backend, defaultOptions);
    this.auth = injector.get(AuthService);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
      return super.request(url, options);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<any> {
      this.requestInterceptor();
      return super.get(this.getFullUrl(url), this.requestOptions(options))
          .catch(this.onCatch.bind(this))
          .do((res: Response) => {
              this.onSubscribeSuccess(res);
          }, (error: any) => {
              this.onSubscribeError(error);
          })
          .finally(() => {
              this.onFinally();
          });
  }

  getLocal(url: string, options?: RequestOptionsArgs): Observable<any> {
      return super.get(url, options);
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
      this.requestInterceptor();
      return super.post(this.getFullUrl(url), body, this.requestOptions(options))
          .catch(this.onCatch.bind(this))
          .do((res: Response) => {
              this.onSubscribeSuccess(res);
          }, (error: any) => {
              this.onSubscribeError(error);
          })
          .finally(() => {
              this.onFinally();
          });
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
      this.requestInterceptor();
      return super.patch(this.getFullUrl(url), body, this.requestOptions(options))
          .catch(this.onCatch.bind(this))
          .do((res: Response) => {
              this.onSubscribeSuccess(res);
          }, (error: any) => {
              this.onSubscribeError(error);
          })
          .finally(() => {
              this.onFinally();
          });
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
      this.requestInterceptor();
      return super.put(this.getFullUrl(url), body, this.requestOptions(options))
          .catch(this.onCatch.bind(this))
          .do((res: Response) => {
              this.onSubscribeSuccess(res);
          }, (error: any) => {
              this.onSubscribeError(error);
          })
          .finally(() => {
              this.onFinally();
          });
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<any> {
      this.requestInterceptor();
      return super.delete(this.getFullUrl(url), this.requestOptions(options))
          .catch(this.onCatch.bind(this))
          .do((res: Response) => {
              this.onSubscribeSuccess(res);
          }, (error: any) => {
              this.onSubscribeError(error);
          })
          .finally(() => {
              this.onFinally();
          });
  }

  private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
      // :TODO add headers for every request if not provided
      if (!options) {
        options = new RequestOptions();
      }
      options.headers = this.addAuthorizationHeader(options.headers);
      return options;
  }

  private addAuthorizationHeader(headers: Headers): Headers {
    // add authorization key for requests if available
    if (!headers) {
      headers = new Headers();
    }
    headers.append("Authorization", this.auth.getAuthToken());
    headers.append("Content-Type", "application/json");
    return headers;
  }

  // we can set base url for api calling here also
  private getFullUrl(url: string): string {
      // return full URL to API here
      return url;
  }

  // to load anything in interceptor
  private requestInterceptor(): void {
    // start loaders here
    if (this.auth.getAuthToken()) {
      if (this.helper.isTokenExpired(this.auth.getAuthToken())) {
        this.auth.logout();
      }
    }
  }

  // to do anything in response of interceptor
  private responseInterceptor(): void {
      // revoke loaders here
  }

  // catch error remove token and redirect user to login
  onCatch(err: any, caught: Observable<any>): Observable<any> {
    if (err.status === 401) {
      this.redirectUser();
    }
    return Observable.throw(err);
  }

  redirectUser(): void {
    // TODO: temporary fix for router not being injected and getting
    // undefined. Need to investigate!
    this.auth.logout();
  }

  // successfully subscribed
  private onSubscribeSuccess(res: Response): void { }

  // error in successfully subscribed
  private onSubscribeError(error: any): void { }

  // finally called
  private onFinally(): void {
      this.responseInterceptor();
  }
}

// This is to make happy the AoT compiler
export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, injector: Injector) {
  return new HttpInterceptor(xhrBackend, requestOptions, injector);
}
