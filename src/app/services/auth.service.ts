import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { JwtHelper } from "angular2-jwt";
import { environment } from "../../environments/environment";
import { Observable, Subscription } from "rxjs";
import { UserModel } from "../models";

@Injectable()
export class AuthService {
	AUTH_KEY: string = "buddy-web.jwt";
	private user: UserModel = null;
  private helper: JwtHelper = new JwtHelper();

  private authObserable: Observable<any>;
  private authSubscription: Subscription;

	constructor(private router: Router) {}

	getAPI(): string {
    return environment.API;
  }

	setAuthToken(token: string): void {
		localStorage.setItem(this.AUTH_KEY, token);
    if (this.authSubscription) this.authSubscription.unsubscribe();
		this.setAuthObserable();
  }
  
  setAuthUser(user: UserModel): void {
    this.user = user;
  }

  removeAuthUser(): void {
    this.user = null;
  }

	getAuthToken(): string {
		return localStorage.getItem(this.AUTH_KEY);
	}

	removeAuthToken(): void {
		localStorage.removeItem(this.AUTH_KEY);
	}

	isLogin(): boolean {
    return (this.getAuthToken() ? true : false);
  }

	logout(): void {
    this.removeLogin();
    this.router.navigate(["login"]);
  }

	setAuthObserable(): void {
    if (this.authSubscription || !this.isLogin()) {
      return;
    }
    let decoded = this.helper.decodeToken(this.getAuthToken());
    let now = new Date().getTime();
    let timeout = decoded.exp * 1000 - now - 3600000;
    this.authObserable = Observable.timer(timeout);
    this.authSubscription = this.authObserable.subscribe(expired => {
      this.logout();
    });
  }

	private removeLogin(): void {
    this.removeAuthToken();
    this.user = null;
    if (this.authSubscription) this.authSubscription.unsubscribe();
    this.authObserable = null;
  }
}
