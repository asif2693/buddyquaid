import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
  
  constructor(private router: Router, private auth: AuthService) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    let path: string = next.url.length ? next.url[0].path : "";
    if ((["login", "signup"].indexOf(path) === -1) && (!this.auth.isLogin())) {
      this.auth.logout();
      return false;
    } else if ((["login", "signup"].indexOf(path) >= 0) && (this.auth.isLogin())) {
      this.router.navigate(["/"]);
      return false;
    } else {
      return true;
    }
  }
}
