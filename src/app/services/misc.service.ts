import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel } from "../models";
import { environment } from "../../environments/environment";
import { AuthService } from "./auth.service";

@Injectable()
export class MiscService {
	private API: string = "";

	constructor(private http: Http, private auth: AuthService) {
    this.API = this.auth.getAPI();
  }

  getFaqs(): Observable<any> {
    return this.http.get(`${this.API}/faqs`)
    .map(res => res.json());
  }

  getPrivacy(): Observable<any> {
    return this.http.get(`${this.API}/privacy`)
    .map(res => res.json());
  }

  sendContactMsg(body: ContactMSG): void {
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    for (let key in body) {
      formData.append(key, body[key]);
    }
    xhr.open("POST", `${environment.EMAIL_API}/contact.php`, true);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {}
    };
    xhr.send(formData);
  }
}

export interface ContactMSG {
  name: string;
  email: string;
  phone: string;
  subject: string;
  msg: string;
}
