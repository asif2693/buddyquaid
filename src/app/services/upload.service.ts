import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription, Observer } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel } from "../models";
import { AuthService } from "./auth.service";

@Injectable()
export class UploadService {
	private API: string = "";
  private user: UserModel = null;
  private userRequest = null;

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/upload`;
  }

  upload(file: any): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      let xhr = new XMLHttpRequest();
      let formData = new FormData();
      formData.append("file", file);
      xhr.open("POST", `${this.API}`, true);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          observer.next(JSON.parse(xhr.response));
          observer.complete();
        }
      };
      xhr.send(formData);
    });
  }
}
