import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription, BehaviorSubject } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel } from "../models";
import { AuthService } from "./auth.service";

@Injectable()
export class SettingService {
  private API: string = "";
  private settings: Setting[] = [];
	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/settings`;
  }

  getSettings(force?: boolean): Observable<Setting[]> {
    if (this.settings.length > 0 && !force) {
      return Observable.of(this.settings);
    }
    return this.http.get(this.API)
    .map(res => res.json())
    .map(res => {
      this.settings = res;
      return res;
    });
  }
}

export interface Setting {
  _id?: string;
  type: string;
  title: string;
  key?: string;
}
