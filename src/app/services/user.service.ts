import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/share";
import { UserModel } from "../models";
import { AuthService } from "./auth.service";

@Injectable()
export class UserService {
	private API: string = "";
  private user: UserModel = null;
  private userRequest = null;

	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/users`;
  }

  fetchAuthUser(forced?: boolean): Observable<UserModel> {
    return this.http.get(`${this.API}/me`)
    .map(res => res.json())
    .map(res => {
      this.user = res;
      return res;
    });
  }

  fetchUserProfile(id: string): Observable<any> {
    return this.http.get(`${this.API}`, {
      params: {
        user_id: id
      }
    }).map(res => res.json())
    .map(res => {
      return res;
    });;
  }
}
