import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../services";
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs/Observable";
import { UserModel } from "../../models";

@Injectable()
export class SignupService {
  private API: string;
  private EAPI: string;
	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/auth`;
    this.EAPI = environment.EMAIL_API;
  }

  signup(body: any): Observable<UserModel> {
    return this.http.post(`${this.API}/signup`, body).map(res => res.json())
  }

  sendWelcomeEmail(body: SignupEmailModel): void {
    let xhr = new XMLHttpRequest();
    let formData = new FormData();
    for (let key in body) {
      formData.append(key, body[key]);
    }
    xhr.open("POST", `${this.EAPI}/signup-email.php`, true);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {}
    };
    xhr.send(formData);
  }
}

export interface SignupEmailModel {
  s_email: boolean;
  to: string;
  c_name: string;
  link: string;
}
