import { Component, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { SignupService, SignupEmailModel } from "./signup.service";
import { AuthService } from "../../services";
import Swal from "sweetalert2";

@Component({
  selector: "app-signup",
  templateUrl: "signup.html"
})
export class SignupComponent implements OnDestroy {
  form: FormGroup;
  acceptTerms: boolean = false;
  error: boolean = false;
  constructor(
    private fb: FormBuilder,
    private service: SignupService,
    private router: Router,
    private authService: AuthService
  ) {
    this.init();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      username: ["", Validators.required],
      email: ["", Validators.required],
      brokerage : ["", Validators.required],
      phone: ["", Validators.required],
      password : ["", Validators.required],
      date: [new Date().getTime(), Validators.required],
      user_type: ["agent", Validators.required],
      newsletter: [true, Validators.required],
    });
  }

  onAcceptTermsChange(): void {
    this.acceptTerms = !this.acceptTerms;
  }

  onSignup(): void {
    if (!this.acceptTerms) {
      Swal("Error", "Please accept terms & conditions", "error");
      return;
    }
    if (this.form.valid) {
      let req: any = {};
      for (let key in this.form.controls) {
        req[key] = this.form.controls[key].value;
      }
      this.service.signup(req).subscribe(res => {
        this.authService.setAuthToken(res.token);
        this.authService.setAuthUser(res);
        this.sendWelcomeEmail(res);
        // setTimeout(() => this.router.navigate(["payment"]));
        Swal("Success", "Verification email has been sent! please check your inbox", "success");
        this.initForm();
      }, err => {
        let res = JSON.parse(err._body);
        Swal("Error", res.msg, "error");
      });
    } else {
      Swal("Error", "Please fill form correctly", "error");
    }
  }

  sendWelcomeEmail(data): void {
    let body: SignupEmailModel = {
      s_email: true,
      link: `${location.origin}/payment/${data.verify_link}`,
      to: data.email,
      c_name: data.firstname
    };
    this.service.sendWelcomeEmail(body);
  }

  init(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.add("bg");
  }

  ngOnDestroy(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.remove("bg");
  }
}
