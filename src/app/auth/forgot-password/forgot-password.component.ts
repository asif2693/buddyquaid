import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ForgotPasswordService, ForgotPassEmail } from "./forgot-password.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-forgot-password",
  templateUrl: "forgot-password.html"
})
export class ForgotPasswordComponent {
  email: string = "";
  processing: boolean = false;
  sent: boolean = false;
  constructor(private service: ForgotPasswordService, private router: Router) {
    this.init();
  }

  sendEmail(): void {
    this.processing = true;
    this.service.getInfo(this.email).subscribe(res => {
      let body: ForgotPassEmail = {
        s_email: true,
        to: res.email,
        link: `${location.origin}/reset-password/${res.forgot_link}`,
        c_name: res.firstname
      };
      this.service.sendEmail(body).subscribe(sent => {
        Swal("Success", "Email sent for password recovery!", "success");
        this.sent = true;
        this.processing = false;
      });
    }, err => {
      let res = JSON.parse(err._body);
      Swal("Error", res.msg, "error");
      this.processing = false;
    });
  }


  init(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.add("bg");
  }
}
