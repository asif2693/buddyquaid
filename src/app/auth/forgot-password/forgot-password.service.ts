import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../services";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../environments/environment";
import { Observer } from "rxjs";

@Injectable()
export class ForgotPasswordService {
  private API: string;
	constructor(private http: Http, private auth: AuthService) {
    this.API = environment.EMAIL_API;
  }

  getInfo(email: string): Observable<any> {
    return this.http.post(`${this.auth.getAPI()}/auth/forgot-password`, {
      email: email
    }).map(res => res.json());
  }

  sendEmail(body: ForgotPassEmail): Observable<any> {
    return Observable.create((o: Observer<any>) => {
      let xhr = new XMLHttpRequest();
      let formData = new FormData();
      for (let key in body) {
        formData.append(key, body[key]);
      }
      xhr.open("POST", `${this.API}/forgot-pass.php`, true);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          o.next(true);
          o.complete();
        }
      };
      xhr.send(formData);
    });
  }
}

export interface ForgotPassEmail {
  s_email: boolean;
  to: string;
  c_name: string;
  link: string;
}
