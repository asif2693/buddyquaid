import { Component, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ResetPasswordService } from "./reset-password.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-reset-password",
  templateUrl: "reset-password.html"
})
export class ResetPasswordComponent implements OnDestroy {
  form: FormGroup;
  link: string = "";
  sub: any;
  done: string = "";
  constructor(
    private fb: FormBuilder,
    private service: ResetPasswordService,
    private router: Router,
    private activated: ActivatedRoute
  ) {
    this.init();
    this.initForm();
    this.sub = this.activated.params.subscribe(params => {
      if (params.id) {
        this.link = params.id;
      }
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      password: ["", Validators.required],
      cpassword: ["", Validators.required]
    });
  }

  init(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.add("bg");
  }

  onChangePass(): boolean {
    if (!this.form.valid) {
      return;
    }
    if (this.form.controls["password"].value.length && this.form.controls["password"].value == this.form.controls["cpassword"].value) {
      this.service.resetPassword(this.form.controls["password"].value, this.link).subscribe(res => {
        Swal("Success", "Password updated successfully!", "error");
        this.done = "Password updated successfully!";
      }, err => {
        Swal("Error", "Recovery link is expired", "error");
        this.done = "Recovery link is expired!";
      });
    } else {
      this.done = "Recovery link is expired!";
      Swal("Error", "Please enter valid password", "error");
    }
  }

  ngOnDestroy(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.remove("bg");
    this.sub.unsubscribe();
  }
}
