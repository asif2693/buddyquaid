import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../services";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ResetPasswordService {
  private API: string;
	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/auth`;
  }

  resetPassword(password: string, forgot_link: string): Observable<any> {
    return this.http.post(`${this.API}/reset-password`, {
      forgot_link: forgot_link,
      password: password
    }).map(res => res.json());
  }
}
