import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { HttpModule } from "@angular/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { LoginComponent, LoginService } from "./login";
import { VerifyEmailComponent, VerifyEmailService } from "./verify-email";
import { SignupComponent, SignupService } from "./signup";
import { ResetPasswordComponent, ResetPasswordService } from "./reset-password";
import { ForgotPasswordComponent, ForgotPasswordService } from "./forgot-password";
import { SectionsModule } from "../pages";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    SectionsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent
  ],
  exports: [],
  providers: [
    LoginService,
    SignupService,
    ResetPasswordService,
    ForgotPasswordService,
    VerifyEmailService
  ]
})
export class AuthModule { }
