import { Component, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { VerifyEmailService } from "./verify-email.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-verify-email",
  templateUrl: "verify-email.html"
})
export class VerifyEmailComponent implements OnDestroy {
  sub: any;
  processing: boolean = true;
  constructor(
    private service: VerifyEmailService,
    private router: Router,
    private activated: ActivatedRoute
  ) {
    this.init();
    this.sub = this.activated.params.subscribe(params => {
      if (params.id) {
        this.verifyEmail(params.id);
      }
    });
  }

  verifyEmail(link): void {
    this.service.verifyUserEmail(link).subscribe(res => {
      this.processing = false;
    });
  }

  init(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.add("bg");
  }

  ngOnDestroy(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.remove("bg");
    this.sub.unsubscribe();
  }
}
