import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../services";
import { Observable } from "rxjs/Observable";
import { environment } from "../../../environments/environment";
import { Observer } from "rxjs";

@Injectable()
export class VerifyEmailService {
  private API: string;
	constructor(private http: Http, private auth: AuthService) {
    this.API = environment.EMAIL_API;
  }

  verifyUserEmail(link: string): Observable<any> {
    return this.http.post(`${this.auth.getAPI()}/auth/confirm`, {
      verify_link: link
    }).map(res => res.json());
  }
}

