import { Component, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginService } from "./login.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-login",
  templateUrl: "login.html"
})
export class LoginComponent implements OnDestroy {
  form: FormGroup;
  error: boolean = false;
  constructor(private fb: FormBuilder, private service: LoginService, private router: Router) {
    this.init();
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });
  }

  init(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.add("bg");
  }

  onLogin(): boolean {
    if (!this.form.valid) {
      return;
    }
    this.service.authenticate(this.form.controls["email"].value, this.form.controls["password"].value)
    .subscribe(res => {
      this.service.setToken(res.token);
      if (res.isPaymentAdded) {
        this.router.navigate(["/payment"]);
      } else {
        this.router.navigate(["/"]);
      }
    }, err => {
      let res = JSON.parse(err._body);
      Swal("Error", res.msg, "error");
      this.error = true;
    });
    return false;
  }

  ngOnDestroy(): void {
    let body: Element = document.getElementsByTagName("body")[0];
    body.classList.remove("bg");
  }
}
