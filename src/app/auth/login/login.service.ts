import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../services";
import { Observable } from "rxjs/Observable";

@Injectable()
export class LoginService {
  private API: string;
	constructor(private http: Http, private auth: AuthService) {
    this.API = `${this.auth.getAPI()}/auth`;
  }

  authenticate(email: string, password: string): Observable<any> {
    return this.http.post(`${this.API}/login`, {
      email: email,
      password: password
    }).map(res => res.json())
    .catch(this.onLoginError.bind(this));
  }

  setToken(token: string): void {
    this.auth.setAuthToken(token);
  }

  onLoginError(res: Response): Observable<any> {
    return Observable.throw(res);
  }
}
