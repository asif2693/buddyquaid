export interface MessageModel {
  _id?: string;
  firstname: string;
  lastname: string;
  email: string;
  profile_image: string;
  title: string;
  message: string;
  date: string | Date;
}
