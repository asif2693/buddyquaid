export interface UserModel {
  _id?: string;
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  broker: string;
  brokerage : string;
  phone: string;
  verified: string;
  password : string;
  date: string;
  user_type: string;
  profile_image: string;
  newsletter: string;
  isPaymentAdded: boolean;
  token?: string;
  paymentMethod: PaymentMethodModel;
}

export interface PaymentMethodModel {
  pay_source?: string;
  method: string;
  number: string;
  exp_month: string;
  exp_year: string;
  csv: string;
  firstname: string;
  lastname: string;
  email: string;
  address: string;
  city: string;
  state: string;
  zip: string;
}
