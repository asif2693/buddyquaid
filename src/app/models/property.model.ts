export interface PropertyModel {
  _id?: string;
  user_id: string;
  firstname: string;
  lastname: string;
  profile_image: string;
  email: string;
  phone: string;
  title : string;
  location : string;
  price_upon_request: boolean;
  status : string;
  prop_status : string;
  price : string;
  lease: boolean;
  photos : string[],
  size_prefix : string;
  bedrooms : string;
  bathrooms : string;
  tv_launge : string;
  garages : string;
  no_swiming_pool : string;
  description : string;
  features : string[],
  video_url : string;
  year_built: string;
  place_on_map : string;
  date: string;
  city : string;
  acres: string;
  mls : string;
  prop_type : string;
}
