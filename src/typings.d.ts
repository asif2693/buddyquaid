/* SystemJS module definition */
declare var module: NodeModule;
declare var $: any;
declare var initNSTSlider: any;
declare var initPropertyCarousel: any;
declare var initDropZone: any;
declare var Stripe: any;
declare var resetNSTSlider: any;
interface NodeModule {
  id: string;
}
