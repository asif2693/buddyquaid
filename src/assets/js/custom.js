function myFunction() {
  window.print();
}


function initNSTSlider() {
  //********* Range Slider
  $('.nstSlider').nstSlider({
    "rounding": {
      "1000": "15000",
      "500000": "200000000"
    },
    "left_grip_selector": ".leftGrip",
    "right_grip_selector": ".rightGrip",
    "value_bar_selector": ".bar",
    "value_changed_callback": function(cause, leftValue, rightValue) {
      $(this).parent().find('.leftLabel').text(leftValue);
      $(this).parent().find('.rightLabel').text(rightValue);
    }
  });
  $('.nstSlider1').nstSlider({
    "left_grip_selector": ".leftGrip",
    "right_grip_selector": ".rightGrip",
    "value_bar_selector": ".bar",
    "value_changed_callback": function(cause, leftValue, rightValue) {
      $(this).parent().find('.leftLabel').text(leftValue);
      $(this).parent().find('.rightLabel').text(rightValue);
    }
  });
}

function resetNSTSlider() {
  $('.nstSlider').nstSlider("teardown");
  $('.nstSlider1').nstSlider("teardown");
  initNSTSlider();
}

// *********SHOW FILTER
$('.more-filter').on("click", function() {
  $('.more-filter').toggleClass('show-more');
  $('.more-filter .text-1').toggleClass('hide');
  $('.more-filter .text-2').toggleClass('hide');
});

// *********gallary

$(".navbar-toggler").click(function () {
  $(".navbar ").toggleClass("menu-open");
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

function center(number) {
  //Property Details
  var sync2 = $("#property-d-1-2");
  var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
  var num = number;
  var found = false;
  for (var i in sync2visible) {
    if (num === sync2visible[i]) {
      var found = true;
    }
  }
  if (found === false) {
    if (num > sync2visible[sync2visible.length - 1]) {
      sync2.trigger("owl.goTo", num - sync2visible.length + 2)
    } else {
      if (num - 1 === -1) {
        num = 0;
      }
      sync2.trigger("owl.goTo", num);
    }
  } else if (num === sync2visible[sync2visible.length - 1]) {
    sync2.trigger("owl.goTo", sync2visible[1])
  } else if (num === sync2visible[0]) {
    sync2.trigger("owl.goTo", num - 1)
  }
}

function syncPosition(el) {
  var current = this.currentItem;
  $("#property-d-1-2").find(".owl-item").removeClass("synced").eq(current).addClass("synced")
  if ($("#property-d-1-2").data("owlCarousel") !== undefined) {
    center(current)
  }
}

function initPropertyCarousel() {
  //Property Details
  var sync1 = $("#property-d-1");
  var sync2 = $("#property-d-1-2");
  sync1.owlCarousel({
    autoPlay: false,
    singleItem: true,
    slideSpeed: 1000,
    transitionStyle: "fade",
    navigation: true,
    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    pagination: false,
    afterAction: syncPosition,
    responsiveRefreshRate: 200,
  });
  sync2.owlCarousel({
    autoPlay: false,
    items: 4,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [979, 6],
    itemsTablet: [768, 5],
    itemsMobile: [479, 3],
    pagination: false,
    responsiveRefreshRate: 100,
    afterInit: function(el) {
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });

  $("#property-d-1-2").on("click", ".owl-item", function(e) {
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo", number);
  });
}
